package es.slateapp.android.app

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import es.slateapp.android.app.prefs.getAccount
import eu.amirs.JSON

class autoload : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val prefs = this.getSharedPreferences("prefs", 0) // obtiene las preferencias del usuario
        val welcome = prefs!!.getBoolean("welcomeShown", false) // obtiene la variable que almacena si se ha mostrado la bienvenida a la aplicación
        val account = getAccount(this) // obtiene la cuenta que se está utilizando

        if (!welcome){
            val intent = Intent(this, es.slateapp.android.app.welcome.welcome::class.java)
            startActivity(intent)
        } else if (JSON(account).key("uuid").isNull||JSON(account).key("hash").isNull) {
            val intent = Intent(this, es.slateapp.android.app.welcome.login::class.java)
            startActivity(intent)
        } else {
            val intent = Intent(this, es.slateapp.android.app.dashboard.cp::class.java)
            startActivity(intent)
        }

        finish()



    }
}
