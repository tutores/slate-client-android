package es.slateapp.android.app.dashboard

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import es.slateapp.android.app.R
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.location.places.ui.PlaceAutocomplete.getStatus
import com.google.android.gms.location.places.Place
import android.content.Intent
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import es.slateapp.android.app.prefs.getAccount
import eu.amirs.JSON
import org.jetbrains.anko.*


class centreCreator : AppCompatActivity() {

    val PLACE_AUTOCOMPLETE_REQUEST_CODE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_centre_creator)


        try {
            val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                .build(this)
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
        } catch (e: GooglePlayServicesRepairableException) {
            finish()
        } catch (e: GooglePlayServicesNotAvailableException) {
            finish()
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val place = PlaceAutocomplete.getPlace(this, data!!)
                val placeid = place.id

                val account = JSON(getAccount(this@centreCreator))
                "https://slateapp.es/b/v1/call/createcentre.php?uuid=${account.key("uuid").stringValue()}&hash=${account.key("hash").stringValue()}&centrelocation=$placeid".httpGet().responseString { request, response, result ->
                    //do something with response
                    when (result) {
                        is Result.Failure -> {
                            val ex = result.getException()
                            alert("Ha ocurrido un error: ${ex.response}","¡Woops!"){
                                okButton { finish() }
                            }.show()
                        }
                        is Result.Success -> {

                            val data = result.get()
                            val json = JSON(data)

                            if (!json.key("uuid").isNull){

                                finish()

                            } else {

                                alert("Ha ocurrido un error: ${json.key("error").intValue()}","¡Woops!"){
                                    okButton { finish() }
                                }.show()

                            }
                        }
                    }
                }

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                finish()

            } else if (resultCode == Activity.RESULT_CANCELED) {
                finish()
            }
        }
    }
}
