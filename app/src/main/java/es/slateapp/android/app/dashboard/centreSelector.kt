package es.slateapp.android.app.dashboard

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import es.slateapp.android.app.R
import es.slateapp.android.app.autoload
import es.slateapp.android.app.prefs.deleteAccount
import es.slateapp.android.app.prefs.getAccount
import eu.amirs.JSON
import kotlinx.android.synthetic.main.activity_centre_selector.*
import org.jetbrains.anko.support.v4.onRefresh
import android.content.Context
import android.os.Build
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.ImageView
import com.balysv.materialripple.MaterialRippleLayout
import com.squareup.picasso.Picasso
import es.slateapp.android.app.dashboard.profiles.centreInfo
import es.slateapp.android.app.prefs.selectCentre
import org.jetbrains.anko.*


class centreSelector : AppCompatActivity() {

    fun convertDpToPx(context: Context, dp: Float): Int { // http://www.androidtutorialshub.com/android-convert-dp-px-px-dp/
        return (dp * context.resources.displayMetrics.density).toInt()
    }

    private fun addCard(uuid: String, role: String, name: String, owner: String, creation: String, location: String, picurl: String, imgauthor: String, imgattributions: String){
        val rippleview = MaterialRippleLayout(this@centreSelector)
        rippleview.backgroundColorResource=R.color.colorPrimary
        rippleview.setRippleOverlay(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rippleview.elevation=convertDpToPx(this@centreSelector, 4.toFloat()).toFloat()
        }
        rippleview.layoutParams=RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,convertDpToPx(this@centreSelector,150.toFloat()))
        (rippleview.layoutParams as RelativeLayout.LayoutParams).setMargins(0,0,0,convertDpToPx(this@centreSelector, 10.toFloat()))

        val relativelayout = RelativeLayout(this@centreSelector)
        relativelayout.layoutParams=RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT)

        val morevert = Button(this@centreSelector)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            morevert.background=getDrawable(R.color.mtrl_btn_transparent_bg_color)
        }
        morevert.layoutParams=RelativeLayout.LayoutParams(wrapContent, wrapContent)
        val params = morevert.layoutParams as RelativeLayout.LayoutParams
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)

        morevert.layoutParams = params //causes layout update
        val img = this@centreSelector.resources.getDrawable( R.drawable.more_vertical)
        img.setBounds(0,0,0,0)
        morevert.setCompoundDrawablesWithIntrinsicBounds(null,null, img,null)
        (morevert.layoutParams as RelativeLayout.LayoutParams).setMargins(0,0,convertDpToPx(this@centreSelector, 10.toFloat()),0)

        val pic = ImageView(this@centreSelector)
        pic.layoutParams=RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT)
        pic.scaleType = ImageView.ScaleType.CENTER_CROP

        Picasso.get().load(picurl).into(pic)

        relativelayout.addView(pic)
        relativelayout.addView(morevert)
        rippleview.addView(relativelayout)

        pic.setOnClickListener {
            selectCentre(this@centreSelector, uuid)
            val intent = Intent(this, es.slateapp.android.app.autoload::class.java)
            startActivity(intent)
            finish()
        }

        morevert.setOnClickListener {
            val intent = Intent(this@centreSelector, centreInfo::class.java)
            intent.putExtra("uuid", uuid)
            intent.putExtra("role", role)
            intent.putExtra("name", name)
            intent.putExtra("creation", creation)
            intent.putExtra("location", location)
            intent.putExtra("img", picurl)
            intent.putExtra("imgauthor", imgauthor)
            intent.putExtra("imgattributions", imgattributions)
            startActivity(intent)
        }

        pic.setOnLongClickListener {
            val intent = Intent(this@centreSelector, centreInfo::class.java)
            intent.putExtra("uuid", uuid)
            intent.putExtra("role", role)
            intent.putExtra("name", name)
            intent.putExtra("creation", creation)
            intent.putExtra("location", location)
            intent.putExtra("img", picurl)
            intent.putExtra("imgauthor", imgauthor)
            intent.putExtra("imgattributions", imgattributions)
            startActivity(intent)
            true
        }

        list.addView(rippleview)
    }

    private fun updateList(account: JSON){
        scrollwrap.isRefreshing=true

        scroll.animate().setDuration(100).alpha(0.2.toFloat())

        placeholder.visibility=View.GONE
        loaded.visibility=View.GONE
        "https://slateapp.es/b/v1/call/getcentres.php?uuid=${account.key("uuid").stringValue()}&hash=${account.key("hash").stringValue()}".httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    scroll.animate().setDuration(100).alpha(1.toFloat())
                    scrollwrap.isRefreshing=false
                    val ex = result.getException()
                    alert("Ha ocurrido un error: ${ex.response}","¡Woops!"){
                        okButton { updateList(account) }
                    }.show()
                }
                is Result.Success -> {

                    scroll.animate().setDuration(100).alpha(1.toFloat())
                    scrollwrap.isRefreshing=false
                    loaded.visibility=View.VISIBLE

                    val data = result.get()
                    val json = JSON(data)

                    if (json.key("error").isNull){

                        list.removeAllViews()
                        val count = json.count()

                        if (count>=1){
                            for (i in 1..count){
                                val centre = json.index(i-1)
                                addCard(centre.key("uuid").stringValue(), centre.key("role").stringValue(), centre.key("name").stringValue(), centre.key("owner").stringValue(), centre.key("creation").stringValue(), centre.key("location").stringValue(), centre.key("img").stringValue(), centre.key("imgauthor").stringValue(), centre.key("imgattributions").stringValue())
                            }
                        } else {
                            placeholder.visibility=View.VISIBLE
                        }

                    } else {
                        scrollwrap.isRefreshing=false
                        loaded.visibility=View.GONE

                        if (json.key("error").isNull){
                            deleteAccount(this@centreSelector)
                            val intent = Intent(this@centreSelector, autoload::class.java)
                            startActivity(intent)
                            finish()
                        } else {
                            alert("Ha ocurrido un error: ${json.key("error").intValue()}","¡Woops!"){
                                okButton { updateList(account) }
                            }.show()
                        }

                    }
                }
            }
        }
    }

    private fun useCode(code:String){
        val account = JSON(getAccount(this@centreSelector))
        "https://slateapp.es/b/v1/call/useinvitationcode.php?uuid=${account.key("uuid").stringValue()}&hash=${account.key("hash").stringValue()}&code=$code".httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    scroll.animate().setDuration(100).alpha(1.toFloat())
                    scrollwrap.isRefreshing=false
                    val ex = result.getException()
                    alert("Ha ocurrido un error: ${ex.response}","¡Woops!"){
                        okButton { updateList(account) }
                    }.show()
                }
                is Result.Success -> {

                    scroll.animate().setDuration(100).alpha(1.toFloat())
                    scrollwrap.isRefreshing=false
                    loaded.visibility=View.VISIBLE

                    val data = result.get()
                    val json = JSON(data)

                    if (json.key("error").isNull){

                        if (json.key("success").booleanValue()){
                            updateList(account)
                        } else {
                            alert("Ha ocurrido un error","¡Woops!"){
                                okButton { updateList(account) }
                            }.show()
                        }

                    } else {

                        alert("Ha ocurrido un error: ${json.key("error").intValue()}. Comprueba si has escrito bien el código o no te encuentras dentro de el centro.","¡Woops!"){
                            okButton { updateList(account) }
                        }.show()

                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        val account = JSON(getAccount(this@centreSelector))

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_centre_selector)

        scrollwrap.onRefresh {
            updateList(account)
        }
        if (!account.key("uuid").isNull && !account.key("hash").isNull){
            updateList(account)
        } else {
            deleteAccount(this@centreSelector)
            val intent = Intent(this@centreSelector, autoload::class.java)
            startActivity(intent)
            finish()
        }

        create.setOnClickListener {
            val intent = Intent(this@centreSelector, centreCreator::class.java)
            startActivity(intent)
        }

        join.setOnClickListener {
            var view:EditText? = null
            alert {
                title="Escribe el código de invitación"
                customView {
                    view = editText()
                }

                view!!.hint="Código de invitación"

                yesButton { useCode(view!!.text.toString()) }

            }.show()
        }


    }

    override fun onResume() {
        val account = JSON(getAccount(this@centreSelector))
        updateList(account)
        super.onResume()
    }
}
