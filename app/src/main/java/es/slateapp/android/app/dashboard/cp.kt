package es.slateapp.android.app.dashboard

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.squareup.picasso.Picasso
import es.slateapp.android.app.R
import es.slateapp.android.app.prefs.getAccount
import es.slateapp.android.app.prefs.getSelectedCentre
import eu.amirs.JSON
import kotlinx.android.synthetic.main.activity_cp.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast

class cp : AppCompatActivity() {

    private fun selectCentre(){
        val intent = Intent(this, centreSelector::class.java)
        startActivity(intent)
        finish()
    }

    private fun applyCentreDetails(name:String,owner:String,creation:String,location:String,img:String,imgauthor:String,imgattributions:String){
        toolbar.title=name

        Picasso.get().load(img).into(settings_header)
    }

    private fun updateCentreDetails(){
        val account = JSON(getAccount(this@cp))
        "https://slateapp.es/b/v1/call/getcentreinfo.php?uuid=${account.key("uuid").stringValue()}&hash=${account.key("hash").stringValue()}&centre_uuid=${getSelectedCentre(this@cp)}".httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    toast("Ha ocurrido un error")
                    offline.visibility= View.VISIBLE
                }
                is Result.Success -> {

                    val data = result.get()
                    val json = JSON(data)

                    if (json.key("error").isNull){

                        val name = json.key("name").stringValue()
                        val owner = json.key("owner").stringValue()
                        val creation = json.key("creaiton").stringValue()
                        val location = json.key("location").stringValue()
                        val img = json.key("img").stringValue()
                        val imgauthor = json.key("imgauthor").stringValue()
                        val imgattributions = json.key("imgattributions").stringValue()
                        applyCentreDetails(name,owner,creation,location,img,imgauthor,imgattributions)

                    } else {
                        toast("Ha ocurrido un error")
                        offline.visibility= View.VISIBLE

                    }
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        wrapper.animate().setDuration(100).alpha(0.toFloat()).withEndAction {
            when (item.itemId) {
                R.id.assigments -> {
                    settings.visibility=View.GONE
                }
                R.id.data -> {
                    settings.visibility=View.GONE
                }
                R.id.settings -> {
                    settings.visibility=View.VISIBLE
                }
            }
            wrapper.animate().setDuration(100).alpha(1.toFloat())
        }
        return@OnNavigationItemSelectedListener true
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cp)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        if (getSelectedCentre(this@cp) ==null){
            selectCentre()
        }

        toolbar.title= getSelectedCentre(this@cp)

        val toolbar:android.support.v7.widget.Toolbar? = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)


        supportActionBar!!.setHomeAsUpIndicator(R.drawable.compare_arrows)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        updateCentreDetails()

        reload.setOnClickListener {
            selectCentre()
        }

        members.setOnClickListener {
            val intent = Intent(this, es.slateapp.android.app.dashboard.sub.members::class.java)
            startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                selectCentre()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
