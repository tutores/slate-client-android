package es.slateapp.android.app.dashboard.profiles

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import es.slateapp.android.app.R
import android.content.Intent
import android.net.Uri
import android.view.MenuItem
import android.support.v4.app.NavUtils
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.squareup.picasso.Picasso
import es.slateapp.android.app.prefs.getAccount
import eu.amirs.JSON
import kotlinx.android.synthetic.main.activity_centre_info.*
import org.jetbrains.anko.*


class centreInfo : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_centre_info)

        this.supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val uuid= intent.extras.getString("uuid")

        val rolevalue= intent.extras.getString("role")

        when (rolevalue) {
            "4" -> {
                role.text="Eres el/la dueño/a de este centro."
            }
            "3" -> {
                role.text="Eres administrador/a de este centro"
            }
            "2" -> {
                role.text="Eres profesor/a en este centro"
            }
            else -> {
                role.text="Eres alumno/a en este centro"
            }
        }

        val name= intent.extras.getString("name")

        //val owner= intent.extras.getString("owner")

        val creationdata= intent.extras.getString("creation")

        creation.text="Registrado el $creationdata"

        val locationplus= intent.extras.getString("location")

        location.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.codes/$locationplus"))
            startActivity(browserIntent)
        }

        val img= intent.extras.getString("img")

        Picasso.get().load(img).into(header)

        val imgauthor= intent.extras.getString("imgauthor")

        copyright.text="Fotografía por: $imgauthor"

        val imgattributions= intent.extras.getString("imgattributions")

        copyright.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(imgattributions))
            startActivity(browserIntent)
        }

        title=name

        fun leaveCentre(confirmation: Boolean){
            val account = JSON(getAccount(this@centreInfo))
            val request = if (confirmation){
                "https://slateapp.es/b/v1/call/leavecentre.php?removeconfirmation=true&uuid=${account.key("uuid").stringValue()}&hash=${account.key("hash").stringValue()}&centre_uuid=$uuid"
            } else {
                "https://slateapp.es/b/v1/call/leavecentre.php?uuid=${account.key("uuid").stringValue()}&hash=${account.key("hash").stringValue()}&centre_uuid=$uuid"
            }

            request.httpGet().responseString { request, response, result ->
                //do something with response
                when (result) {
                    is Result.Failure -> {
                        val ex = result.getException()
                        alert("Ha ocurrido un error: ${ex.response}","¡Woops!"){
                            okButton { finish() }
                        }.show()
                    }
                    is Result.Success -> {

                        val data = result.get()
                        val json = JSON(data)

                        if (json.key("error").isNull){

                            if (!json.key("success").isNull){

                                toast("Has abandonado $name")
                                finish()

                            } else {
                                alert("Ha ocurrido un error desconocido","¡Woops!"){
                                    okButton { finish() }
                                }.show()
                            }

                        } else {

                            if (json.key("error").intValue()==7){
                                alert("Se eliminaran todos los datos del centro, dado que no has transferido el centro a ningún usuario","¿De verdad?"){
                                    yesButton {
                                        leaveCentre(true)
                                    }
                                    noButton {
                                        toast("Cancelado")
                                    }
                                }.show()
                            } else {
                                alert("Ha ocurrido un error: ${json.key("error").intValue()}","¡Woops!"){
                                    okButton { finish() }
                                }.show()
                            }

                        }
                    }
                }
            }
        }

        leave.setOnClickListener {

            alert("Vas a abandonar $name, ¿Deseas continuar?","¡Cuidado!"){
                yesButton {
                    leaveCentre(false)
                }
                noButton { toast("Cancelado") }
            }.show()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
