package es.slateapp.android.app.dashboard.profiles

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import es.slateapp.android.app.R
import es.slateapp.android.app.prefs.getAccount
import eu.amirs.JSON
import jp.wasabeef.blurry.Blurry
import kotlinx.android.synthetic.main.activity_user_info.*
import org.jetbrains.anko.*





class userInfo : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_info)

        val uuid= intent.extras.getString("uuid")
        this.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title="Ver perfil"

        val account = JSON(getAccount(this@userInfo))


        "https://slateapp.es/b/v1/call/getprofileinfo.php?uuid=${account.key("uuid").stringValue()}&hash=${account.key("hash").stringValue()}&stalking=$uuid".httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    alert("Ha ocurrido un error: ${ex.response}","¡Woops!"){
                        okButton { finish() }
                    }.show()
                }
                is Result.Success -> {

                    val data = result.get()
                    val json = JSON(data)

                    if (json.key("error").isNull){

                        val img = json.key("pic").stringValue()
                        val name = json.key("name").stringValue()
                        val creation = json.key("creation").stringValue()

                        userJoinDate.text=creation
                        userName.text=name

                        title=name

                        Picasso.get().load(img).into(userPic)
                        Picasso.get().load(img).into(userHeader)

                        Picasso.get()
                            .load(img)
                            .into(userHeader, object : com.squareup.picasso.Callback {
                                override fun onSuccess() {
                                    Blurry.with(this@userInfo).capture(userHeader).into(userHeader)
                                }

                                override fun onError(ex: Exception) {
                                    // nothing
                                }
                            })

                        userHeader.alpha=0.5.toFloat()
                        val member = json.key("member")

                        if (member.count()==1){
                            centreImgCentre.visibility= View.VISIBLE
                            Picasso.get().load(member.index(0).key("img").stringValue()).into(centreImgCentre)
                        } else if(member.count()==2){
                            centreImgLeft.visibility= View.VISIBLE
                            centreImgCentre.visibility= View.VISIBLE
                            Picasso.get().load(member.index(0).key("img").stringValue()).into(centreImgLeft)
                            Picasso.get().load(member.index(1).key("img").stringValue()).into(centreImgCentre)
                        } else if(member.count()>=3) {
                            centreImgLeft.visibility= View.VISIBLE
                            centreImgCentre.visibility= View.VISIBLE
                            centreImgRight.visibility= View.VISIBLE
                            Picasso.get().load(member.index(0).key("img").stringValue()).into(centreImgLeft)
                            Picasso.get().load(member.index(1).key("img").stringValue()).into(centreImgCentre)
                        }

                    } else {

                        alert("Ha ocurrido un error: ${json.key("error").intValue()}","¡Woops!"){
                            okButton { finish() }
                        }.show()

                    }
                }
            }
        }


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
