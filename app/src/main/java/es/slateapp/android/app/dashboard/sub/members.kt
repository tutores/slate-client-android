package es.slateapp.android.app.dashboard.sub

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import es.slateapp.android.app.R
import es.slateapp.android.app.prefs.getAccount
import es.slateapp.android.app.prefs.getSelectedCentre
import eu.amirs.JSON
import kotlinx.android.synthetic.main.activity_members.*
import org.jetbrains.anko.alert
import android.util.TypedValue
import android.view.Gravity
import com.squareup.picasso.Picasso
import es.slateapp.android.app.dashboard.profiles.userInfo
import org.jetbrains.anko.backgroundColorResource


class members : AppCompatActivity() {

    var studentsEmpty=true
    var teachersEmpty=true
    var adminsEmpty=true

    fun convertDpToPx(context: Context, dp: Float): Int { // http://www.androidtutorialshub.com/android-convert-dp-px-px-dp/
        return (dp * context.resources.displayMetrics.density).toInt()
    }

    fun spToPx(sp: Float, context: Context): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.resources.displayMetrics).toInt()
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        member_wrapper.animate().setDuration(100).alpha(0.toFloat()).withEndAction {
            when (item.itemId) {
                R.id.navigation_home -> {
                    students.visibility= View.VISIBLE
                    admins.visibility= View.GONE
                    teachers.visibility= View.GONE

                    if(studentsEmpty){
                        empty.visibility=View.VISIBLE
                    } else {
                        empty.visibility=View.GONE
                    }
                }
                R.id.navigation_dashboard -> {
                    students.visibility= View.GONE
                    teachers.visibility= View.VISIBLE
                    admins.visibility= View.GONE

                    if(teachersEmpty){
                        empty.visibility=View.VISIBLE
                    } else {
                        empty.visibility=View.GONE
                    }
                }
                R.id.navigation_notifications -> {
                    students.visibility= View.GONE
                    teachers.visibility= View.GONE
                    admins.visibility= View.VISIBLE

                    if(adminsEmpty){
                        empty.visibility=View.VISIBLE
                    } else {
                        empty.visibility=View.GONE
                    }
                }
            }
            member_wrapper.animate().setDuration(100).alpha(1.toFloat())
        }
        return@OnNavigationItemSelectedListener true
        false
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_members)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        toolbar2.title="Miembros"

        val toolbar:android.support.v7.widget.Toolbar? = findViewById(R.id.toolbar2)
        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        val account = JSON(getAccount(this@members))

        "https://slateapp.es/b/v1/call/getcentrecodes.php?uuid=${account.key("uuid").stringValue()}&hash=${account.key("hash").stringValue()}&centreuuid=${getSelectedCentre(this@members)}".httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    alert("Ha ocurrido un error: ${ex.response}","¡Woops!").show()
                }
                is Result.Success -> {
                    val data = result.get()
                    val json = JSON(data)

                    if (json.key("error").isNull){

                        if(!json.key("students").isNull){
                            val studentsCode = json.key("students").key("default").key("code").stringValue()
                            studentsCodeLabel.text=studentsCode
                            studentsCodeWrapper.visibility=View.VISIBLE
                        }
                        if(!json.key("teachers").isNull){
                            val teachersCode = json.key("teachers").key("default").key("code").stringValue()
                            teachersCodeLabel.text=teachersCode
                            teachersCodeWrapper.visibility=View.VISIBLE
                        }
                        if(!json.key("admins").isNull){
                            val adminsCode = json.key("admins").key("default").key("code").stringValue()
                            adminsCodeLabel.text=adminsCode
                            adminsCodeWrapper.visibility=View.VISIBLE
                        }

                    } else {

                        alert("Ha ocurrido un error: ${json.key("error").intValue()}","¡Woops!").show()

                    }
                }
            }
        }

        fun addCard(profile:JSON, list:LinearLayout){
            val horitzontallinearview = LinearLayout(this@members)
            horitzontallinearview.orientation= LinearLayout.HORIZONTAL

            horitzontallinearview.layoutParams=LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT)
            horitzontallinearview.setPadding(convertDpToPx(this@members, 10.toFloat()),convertDpToPx(this@members, 10.toFloat()),convertDpToPx(this@members, 10.toFloat()),convertDpToPx(this@members, 10.toFloat()))
            horitzontallinearview.gravity=Gravity.CENTER_VERTICAL

            val circularimageview = de.hdodenhof.circleimageview.CircleImageView(this@members)
            circularimageview.layoutParams=LinearLayout.LayoutParams(convertDpToPx(this@members, 60.toFloat()),convertDpToPx(this@members, 60.toFloat()))
            Picasso.get().load(profile.key("pic").stringValue()).into(circularimageview)

            val text = TextView(this@members)
            text.textSize=spToPx(5.toFloat(),this@members).toFloat()
            text.ellipsize=TextUtils.TruncateAt.END
            text.maxLines=1

            val llp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            llp.setMargins(convertDpToPx(this@members, 20.toFloat()), 0, 0, 0) // llp.setMargins(left, top, right, bottom);
            text.layoutParams = llp

            text.text=profile.key("name").stringValue()

            val divider = View(this@members)
            divider.backgroundColorResource=R.color.colorUnselected
            val dlp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,convertDpToPx(this@members, 1.toFloat()))
            divider.layoutParams=dlp

            horitzontallinearview.addView(circularimageview)
            horitzontallinearview.addView(text)
            list.addView(horitzontallinearview)

            horitzontallinearview.setOnClickListener {
                val intent = Intent(this@members, userInfo::class.java)
                intent.putExtra("uuid", profile.key("uuid").stringValue())
                startActivity(intent)
            }
            list.addView(divider)
        }

        "https://slateapp.es/b/v1/call/getcentremembers.php?uuid=${account.key("uuid").stringValue()}&hash=${account.key("hash").stringValue()}&centreuuid=${getSelectedCentre(this@members)}".httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    alert("Ha ocurrido un error: ${ex.response}","¡Woops!").show()
                }
                is Result.Success -> {
                    val data = result.get()
                    val json = JSON(data)

                    if (json.key("error").isNull){


                        val studentscount = json.key("students").count()

                        if (studentscount>=1){
                            studentsEmpty=false
                            empty.visibility=View.GONE
                            for (i in 1..studentscount){
                                val profile = json.key("students").index(i-1)
                                addCard(profile, studentList)
                            }
                        } else {
                            studentsEmpty=true
                        }

                        val teacherscount = json.key("teachers").count()

                        if (teacherscount>=1){
                            teachersEmpty=false
                            empty.visibility=View.GONE
                            for (i in 1..teacherscount){
                                val profile = json.key("teachers").index(i-1)
                                addCard(profile, teacherList)

                            }
                        } else {
                            teachersEmpty=true
                        }

                        val adminscount = json.key("admins").count()

                        if (adminscount>=1){
                            adminsEmpty=false
                            empty.visibility=View.GONE
                            for (i in 1..adminscount){
                                val profile = json.key("admins").index(i-1)
                                addCard(profile, adminList)

                            }
                        } else {
                            adminsEmpty=true
                        }

                    } else {

                        alert("Ha ocurrido un error: ${json.key("error").intValue()}","¡Woops!").show()

                    }
                }
            }
        }


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
