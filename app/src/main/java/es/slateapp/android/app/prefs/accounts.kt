package es.slateapp.android.app.prefs

import android.content.Context

fun addAccount(context: Context, hash: String, uuid: String){
    val prefs = context.getSharedPreferences("accounts", 0) // obtiene las preferencias del usuario
    val editor = prefs!!.edit()
    editor.putString("hash", hash) // guarda el hash
    editor.putString("uuid", uuid) // guarda el uuid
    editor.apply()
}

fun getAccount(context: Context): String {
    val prefs = context.getSharedPreferences("accounts", 0) // obtiene las preferencias del usuario
    val hash = prefs!!.getString("hash", "") // obtiene la variable que almacena el hash
    val uuid = prefs!!.getString("uuid", "") // obtiene la variable que almacena el uuid

    return if (hash=="" || uuid==""){
        "{'uuid':null,'hash':null}"
    } else {
        "{'uuid':'$uuid','hash':'$hash'}"
    }
}

fun deleteAccount(context: Context){
    val prefs = context.getSharedPreferences("accounts", 0) // obtiene las preferencias del usuario
    val editor = prefs!!.edit()
    editor.putString("hash", "") // guarda el hash
    editor.putString("uuid", "") // guarda el uuid
    editor.apply()
}