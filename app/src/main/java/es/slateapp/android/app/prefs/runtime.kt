package es.slateapp.android.app.prefs

import android.content.Context


fun selectCentre(context: Context, centreuuid: String){
    val prefs = context.getSharedPreferences("runtime", 0) // obtiene las variables de ejecucion
    val editor = prefs!!.edit()
    editor.putString("selectedCentre", centreuuid) // guarda el centro seleccionado
    editor.apply()
}

fun unselectCentre(context: Context){
    val prefs = context.getSharedPreferences("runtime", 0) // obtiene las variables de ejecucion
    val editor = prefs!!.edit()
    editor.putString("selectedCentre", null) // deselecciona un centro
    editor.apply()
}

fun getSelectedCentre(context: Context): String? {
    val prefs = context.getSharedPreferences("runtime", 0) // obtiene las variables de ejecucion
    return prefs!!.getString("selectedCentre", null) // devuelve el centro seleccionado
}
