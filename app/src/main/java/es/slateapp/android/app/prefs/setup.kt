package es.slateapp.android.app.prefs

import android.content.Context

fun welcomeShown(context: Context, status: Boolean){
    val prefs = context.getSharedPreferences("prefs", 0) // obtiene las preferencias del usuario
    val editor = prefs!!.edit()
    editor.putBoolean("welcomeShown", status)
    editor.apply()
}