package es.slateapp.android.app.welcome;


import es.slateapp.android.app.R;

public enum ModelObject {

    FIRST(R.string.welcome_first, R.layout.welcome_first),
    SECOND(R.string.welcome_second, R.layout.welcome_second),
    THIRD(R.string.welcome_third, R.layout.activity_login);

    private int mTitleResId;
    private int mLayoutResId;

    ModelObject(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}