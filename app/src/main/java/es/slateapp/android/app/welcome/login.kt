package es.slateapp.android.app.welcome

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.auth.api.signin.GoogleSignIn
import es.slateapp.android.app.R
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import android.content.Intent
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.android.gms.tasks.Task
import com.google.android.gms.common.api.ApiException
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import android.R.attr.key
import es.slateapp.android.app.prefs.addAccount
import eu.amirs.JSON
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.progressDialog


class login : AppCompatActivity() {

    private val RC_SIGN_IN = 10101

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("301769868848-2loutu89gdvhbi4du8foga8kmt6utb9t.apps.googleusercontent.com")
            .requestEmail()
            .build()

        var mGoogleSignInClient = GoogleSignIn.getClient(this@login, gso)

        fun signIn() {
            val signInIntent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }

        sign_in_button.setOnClickListener {
            signIn()
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            val token = account!!.idToken
            val progress = indeterminateProgressDialog("Comunicándose con Slate", "Ping... ¡Pong!")

            progress.show()

            "https://slateapp.es/login/token?access_token=$token".httpGet().responseString { request, response, result ->
                //do something with response
                when (result) {
                    is Result.Failure -> {
                        val ex = result.getException()
                        progress.dismiss()
                        alert("Ha ocurrido un error: ${ex.response}","¡Woops!")
                    }
                    is Result.Success -> {
                        val data = result.get()
                        val json = JSON(data)

                        if (json.key("error").isNull){

                            progress.dismiss()
                            addAccount(this@login,json.key("hash").stringValue(),json.key("uuid").stringValue())

                            val intent = Intent(this@login, es.slateapp.android.app.autoload::class.java)
                            startActivity(intent)
                            finish()

                        } else {

                            progress.dismiss()
                            alert("Ha ocurrido un error: ${json.key("error").intValue()}","¡Woops!")

                        }
                    }
                }
            }

            // done
        } catch (e: ApiException) {
            alert("Por favor, vuelva a intentarlo más tarde", "Ha ocurrido un error: ${e.statusCode}").show()
        }

    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }
}
