package es.slateapp.android.app.welcome

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import es.slateapp.android.app.R
import android.support.v4.view.ViewPager.OnPageChangeListener
import es.slateapp.android.app.autoload
import es.slateapp.android.app.prefs.welcomeShown
import org.jetbrains.anko.toast


class welcome : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)


        val viewPager = findViewById<ViewPager>(R.id.viewpager)
        viewPager.adapter = CustomPagerAdapter(this)


        viewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                if (position>=2){
                    welcomeShown(this@welcome, true)

                    val intent = Intent(this@welcome, es.slateapp.android.app.welcome.login::class.java)
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                    startActivity(intent)
                    finish()
                }
            }
        })

    }
}
